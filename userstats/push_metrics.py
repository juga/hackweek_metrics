#!/usr/bin/env python3
from csv import DictReader

from prometheus_client.core import GaugeMetricFamily, REGISTRY
from prometheus_client import push_to_gateway
import random


with open('../data/userstats-bridge-country-2020-12-29-by-2021-03-29.csv', encoding="utf-8") as csvfile:
    csv_dict_reader = DictReader(csvfile)


class CustomCollector(object):
    def collect(self):
        g = GaugeMetricFamily('tor_metrics_user_count', 'Help text', labels=['bridge', 'country'])

        with open('../data/userstats-bridge-country-2020-12-29-by-2021-03-29.csv', encoding="utf-8") as csvfile:
            csv_dict_reader = DictReader(csvfile)
            for row in csv_dict_reader:
                g.add_metric([row["bridge"], row["country"]], row["userstats"])
        yield g

REGISTRY.register(CustomCollector())

push_to_gateway('localhost:9091', job='dev-push-gateway', registry=REGISTRY)
